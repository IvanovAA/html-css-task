# Task requirements

Make a layout of the homepage.
In the absence of a layout for mobile devices to be independent.
The page should appear correctly in browsers: Google Chrome, IE11 / Edge, Mozilla Firefox.
Be sure to use one of the preprocessors (SASS, LESS, etc.)
Pixel Perfect 1 in 1 is desirable, but not necessary.

# To start a project

Run index.html in your browser
